package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.presentation.*;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

public class MembersApiImpl extends AbstractApi implements MembersApi {

    MembersApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();


    @Override
    public void createChatRoomMember(String chatRoomName, User member, Handler<AsyncResult<Link>> handler) {
        // TODO complete implementation
        handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
    }

    @Override
    public void deleteChatRoomMember(String chatRoomName, String memberId, Handler<AsyncResult<Void>> handler) {
        // TODO complete implementation
        handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
    }

    @Override
    public void readChatRoomMembers(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfUsers>> handler) {
        // TODO complete implementation
        handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
    }
}
