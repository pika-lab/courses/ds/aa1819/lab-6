package it.unibo.sd1819.lab6.webchat.api;


import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfChatRooms;

import java.util.List;


public interface RoomsApi extends Api {

    void createChatRoom(ChatRoom chatRoom, Handler<AsyncResult<Link>> handler);
    

    void deleteChatRoom(String chatRoomName, Handler<AsyncResult<Void>> handler);
    

    void readAllChatRooms(Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfChatRooms>> handler);
    

    void readChatRoom(String chatRoomName, Integer limitMessages, Integer limitMembers, Handler<AsyncResult<ChatRoom>> handler);

    static RoomsApi get(RoutingContext context) {
        return new RoomsApiImpl(context);
    }
}
