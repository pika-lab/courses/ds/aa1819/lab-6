package it.unibo.sd1819.lab6.webchat.routes;

import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.api.MembersApi;
import it.unibo.sd1819.lab6.webchat.api.MessagesApi;
import it.unibo.sd1819.lab6.webchat.exceptions.BadContentError;
import it.unibo.sd1819.lab6.webchat.exceptions.HttpError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.presentation.*;

import java.io.IOException;
import java.util.Optional;

import static it.unibo.sd1819.lab6.webchat.presentation.MIMETypes.*;

public class MessagesPath extends Path {


    public MessagesPath() {
        super("/messages");
    }

    @Override
    protected void setupRoutes() {
        // TODO set up routes here
    }


    private void post(RoutingContext routingContext) {
        final Future<ChatMessage> result = Future.future();
        result.setHandler(responseHandler(routingContext));

        try {
            throw new NotImplementedError();
        } catch (HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void get(RoutingContext routingContext) {
        throw new NotImplementedError();
    }

}
