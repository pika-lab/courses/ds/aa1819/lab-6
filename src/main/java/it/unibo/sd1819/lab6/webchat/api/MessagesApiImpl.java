package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.presentation.ChatMessage;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfMessages;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.util.List;

public class MessagesApiImpl extends AbstractApi implements MessagesApi {

    MessagesApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();


    @Override
    public void createChatRoomMessage(String chatRoomName, ChatMessage newMessage, Handler<AsyncResult<ChatMessage>> handler) {
        // TODO complete implementation
        handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
    }

    @Override
    public void readChatRoomMessages(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfMessages>> handler) {
        // TODO complete implementation
        handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
    }
}
