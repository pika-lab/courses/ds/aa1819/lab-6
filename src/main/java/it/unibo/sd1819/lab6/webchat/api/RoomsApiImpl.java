    package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.*;
import it.unibo.sd1819.lab6.webchat.presentation.*;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class RoomsApiImpl extends AbstractApi implements RoomsApi {


    RoomsApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();

    @Override
    public void createChatRoom(ChatRoom chatRoom, Handler<AsyncResult<Link>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        chatRoom.setOwner(getAuthenticatedUser().get());

        rooms.tryRead(ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.sameChatRoomOf(chatRoom)))
                .thenAcceptAsync(other -> {
                    // TODO complete implementation
                    handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
                });
    }

    @Override
    public void deleteChatRoom(String chatRoomName, Handler<AsyncResult<Void>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        rooms.tryTake(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(room -> {
            if (room.isPresent()) {
                final User owner = room.get().get().getOwner();
                if (owner.sameUserOf(getAuthenticatedUser().get()) || isAuthenticatedUserAtLeast(User.Role.ADMIN)) {
                    // TODO complete implementation
                    handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
                } else {
                    handler.handle(Future.failedFuture(new ForbiddenError()));
                }
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    @Override
    public void readAllChatRooms(Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfChatRooms>> handler) {
        rooms.readAll(ObjectTemplate.anyOfType(ChatRoom.class)).thenAcceptAsync(rooms -> {
            final Stream<ChatRoom> chatRooms = rooms.stream()
                    .map(ObjectTuple::get)
                    .skip(skip)
                    .limit(limit)
                    .filter(r -> r.getName().contains(filter));
            // TODO complete implementation
            handler.handle(Future.failedFuture(new NotImplementedError("implement me")));
        });
    }

    @Override
    public void readChatRoom(String chatRoomName, Integer limitMessages, Integer limitMembers, Handler<AsyncResult<ChatRoom>> handler) {
        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(room -> {
            if (room.isPresent()) {
                final ChatRoom r = new ChatRoom(room.get().get());
                authorizeUserForReadChatRoom(handler, r); // make sure that the user is authorized to access the chat room
                readChatRoomMembersAndMessages(r, limitMessages, limitMembers, handler);
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private void authorizeUserForReadChatRoom(Handler<AsyncResult<ChatRoom>> handler, ChatRoom r) {
        if (!isAuthenticatedUserAtLeast(User.Role.ADMIN)) { // admins can read any chat room
            if (isAuthenticatedUserAtLeast(User.Role.USER)) {
                final User user = getAuthenticatedUser().get();
                if (!r.getOwner().sameUserOf(user) && !r.getMembers().contains(user)) { // user doesn't own the room,
                                                                                        // nor is it a member
                    handler.handle(Future.failedFuture(new ForbiddenError()));
                }
            } else if (r.getAccessLevel().compareTo(ChatRoom.AccessLevel.PUBLIC) > 0) { // room is not public
                handler.handle(Future.failedFuture(new ForbiddenError()));
            }
        }
    }

    private void readChatRoomMembersAndMessages(ChatRoom room, Integer limitMessages, Integer limitMembers, Handler<AsyncResult<ChatRoom>> handler) {
        ObjectSpaces.chatRoomMembers(room.getName()).readAll(ObjectTemplate.anyOfType(User.class)).thenAcceptAsync(members -> {
            room.setMembersFromStream(members.stream().map(ObjectTuple::get).limit(limitMembers));
            room.setMembersCount(members.size());

            ObjectSpaces.chatRoomMessages(room.getName()).readAll(ObjectTemplate.anyOfType(ChatMessage.class)).thenAcceptAsync(messages -> {
                room.setMessagesFromStream(messages.stream().map(ObjectTuple::get).sorted().limit(limitMessages));
                room.setMessagesCount(messages.size());

                handler.handle(Future.succeededFuture(room));
            });
        });
    }
}
