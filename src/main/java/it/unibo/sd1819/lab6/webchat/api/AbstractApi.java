package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.UnauthorizedError;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

abstract class AbstractApi implements Api {
    private final RoutingContext routingContext;

    AbstractApi(RoutingContext routingContext) {
        this.routingContext = Objects.requireNonNull(routingContext);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return routingContext;
    }

    protected Optional<User> getAuthenticatedUser() {
        final Optional<User> credentials;
        if (getAuthorizationHeader() != null) {
            User result;
            try {
                result = User.fromJSON(getAuthorizationHeader());
            } catch (IOException e) {
                try {
                    result = User.fromYAML(getAuthorizationHeader());
                } catch (IOException e1) {
                    try {
                        result = User.fromXML(getAuthorizationHeader());
                    } catch (IOException e2) {
                        result = null;
                    }
                }
            }
            credentials = Optional.ofNullable(result);
            if (credentials.isPresent()) {
                try {
                    final Optional<User> user = ObjectSpaces.users().tryRead(ObjectTemplate.anyOfType(User.class).where(it -> it.sameUserOf(credentials.get()))).get().map(ObjectTuple::get);
                    if (!user.isPresent() || !user.get().getPassword().equals(credentials.get().getPassword())) {
                        throw new UnauthorizedError("Impossible to authenticate the user");
                    }
                    return user;
                } catch (InterruptedException | ExecutionException e) {
                    throw new UnauthorizedError(e);
                }
            }
        } else {
            credentials = Optional.empty();
        }

        return credentials;
    }

    protected boolean isAuthenticatedUserAtLeast(User.Role role) {
        return getAuthenticatedUser().isPresent() && getAuthenticatedUser().get().getRole().compareTo(role) >= 0;
    }

    protected void ensureAuthenticatedUserAtLeast(User.Role role) {
        if (!getAuthenticatedUser().isPresent()) {
            throw new UnauthorizedError();
        }
        if (getAuthenticatedUser().get().getRole().compareTo(role) < 0) {
            throw new ForbiddenError();
        }
    }
}
