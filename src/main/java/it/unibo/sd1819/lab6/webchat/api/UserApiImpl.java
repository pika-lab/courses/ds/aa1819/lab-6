package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.ConflictError;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfUsers;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

class UserApiImpl extends AbstractApi implements UsersApi {

    UserApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }


    private ObjectSpace<User> users = ObjectSpaces.users();

    @Override
    public void createUser(User userData, Handler<AsyncResult<Link>> handler) {
        if (isAuthenticatedUserAtLeast(User.Role.ADMIN)) { // only admins can create new admin users
            userData.setRole(userData.getRole());
        } else {
            userData.setRole(User.Role.USER);
        }

        users.tryRead(ObjectTemplate.anyOfType(User.class).where(it -> it.sameUserOf(userData)))
                .thenAcceptAsync(other -> {
                     if (other.isPresent()) {
                         handler.handle(Future.failedFuture(new ConflictError()));
                     } else {
                         users.write(ObjectTuple.of(userData)).thenRunAsync(() ->
                             handler.handle(Future.succeededFuture(userData.getLink()))
                         );
                     }
                });
    }

    @Override
    public void readAllUsers(Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfUsers>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.ADMIN);

        // beyond this point, we're sure that the client is at least an admin

        users.readAll(
            ObjectTemplate.anyOfType(User.class).where(it -> it.getUsername().contains(filter))
        ).thenAcceptAsync(tuples ->
            handler.handle(
                Future.succeededFuture(new ListOfUsers(tuples.stream().map(ObjectTuple::get).skip(skip).limit(limit)))
            )
        );
    }

    @Override
    public void readUser(String identifier, Handler<AsyncResult<User>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        // beyond this point, we're sure that the client is at least a user

        users.tryRead(
                ObjectTemplate.anyOfType(User.class).where(it -> it.isIdentifiedBy(identifier))
        ).thenAcceptAsync(tuple -> {
            if (tuple.isPresent()) {
                handler.handle(Future.succeededFuture(tuple.get().get()));
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    @Override
    public void updateUser(String identifier, User newUserData, Handler<AsyncResult<User>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        getAuthenticatedUser().ifPresent(u -> {
            if (!u.isIdentifiedBy(identifier)) { // user data can only be updated by its owner
                throw new ForbiddenError();
            }
        });

        users.tryTake( // remove the current user data...
                ObjectTemplate.anyOfType(User.class).where(it -> it.isIdentifiedBy(identifier))
        ).thenAcceptAsync(tuple -> {
            if (tuple.isPresent()) {
                final User updatedUser = mergeUsers(tuple.get().get(), newUserData);
                users.write(ObjectTuple.of(updatedUser)).thenRunAsync(() -> { // ...and insert the new one
                    handler.handle(Future.succeededFuture(updatedUser));
                });
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private User mergeUsers(User currentUser, User newUserData) {
        final User result = new User(currentUser).setPropertiesToNonNullsOf(newUserData.setId(null));
        result.setLinkUrl(getPath().substring(0, getPath().lastIndexOf('/')) + "/" + result.getUsername());
        return result;
    }


}
