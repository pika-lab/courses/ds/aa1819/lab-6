package it.unibo.sd1819.lab6.ts.objects;

import it.unibo.sd1819.lab6.ts.AbstractTupleSpace;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static it.unibo.sd1819.lab6.ts.objects.ObjectTemplate.anyOfType;

public class ObjectSpace<T> extends AbstractTupleSpace<ObjectTuple<T>, ObjectTemplate<? extends T>> {
    private MultiSet<ObjectTuple<T>> tuples = new HashMultiSet<>();

    public ObjectSpace(String name, ExecutorService executor) {
        super(name, executor);
    }


    public ObjectSpace(ExecutorService executor) {
        super(ObjectSpace.class.getName(), executor);
    }

    public ObjectSpace(String name) {
        this(name, globalExecutor());
    }

    public ObjectSpace() {
        this(globalExecutor());
    }

    @Override
    protected Collection<ObjectTuple<T>> lookForTuples(ObjectTemplate<? extends T> template, int limit) {
        return tuples.stream().filter(template).limit(limit).collect(Collectors.toList());
    }

    @Override
    protected Optional<ObjectTuple<T>> lookForTuple(ObjectTemplate<? extends T> template) {
        return tuples.stream().filter(template).findAny();
    }

    @Override
    protected Collection<ObjectTuple<T>> retrieveTuples(ObjectTemplate<? extends T> template, int limit) {
        final List<ObjectTuple<T>> result = new LinkedList<>();
        final Iterator<ObjectTuple<T>> i = tuples.iterator();
        while (i.hasNext()) {
            final ObjectTuple<T> tuple = i.next();
            if (tuple.matches(template)) {
                i.remove();
                result.add(tuple);
            }
        }
        return result;
    }

    @Override
    protected Optional<ObjectTuple<T>> retrieveTuple(ObjectTemplate<? extends T> template) {
        final Iterator<ObjectTuple<T>> i = tuples.iterator();
        while (i.hasNext()) {
            final ObjectTuple<T> tuple = i.next();
            if (tuple.matches(template)) {
                i.remove();
                return Optional.of(tuple);
            }
        }
        return Optional.empty();
    }

    @Override
    protected void insertTuple(ObjectTuple<T> tuple) {
        tuples.add(tuple);
    }

    @Override
    protected Collection<ObjectTuple<T>> getAllTuples() {
        return new ArrayList<>(tuples);
    }

    @Override
    protected int countTuples() {
        return tuples.size();
    }

}