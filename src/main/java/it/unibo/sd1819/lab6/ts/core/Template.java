package it.unibo.sd1819.lab6.ts.core;

public interface Template {
    boolean matches(Tuple tuple);
}
